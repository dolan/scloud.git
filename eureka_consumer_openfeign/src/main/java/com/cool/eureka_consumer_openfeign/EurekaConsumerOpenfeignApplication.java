package com.cool.eureka_consumer_openfeign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
@EnableEurekaClient
public class EurekaConsumerOpenfeignApplication {

    public static void main(String[] args) {
        SpringApplication.run(EurekaConsumerOpenfeignApplication.class, args);
    }

}
