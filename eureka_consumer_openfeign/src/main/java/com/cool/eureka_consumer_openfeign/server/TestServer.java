package com.cool.eureka_consumer_openfeign.server;

import com.cool.eureka_consumer_openfeign.Hystric.EurekaClientServiceHystric;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Component
@FeignClient(value = "eureka-client",fallback = EurekaClientServiceHystric.class)
public interface TestServer {
    @GetMapping(value = "/hello")
    public String sayHello(@RequestParam(value = "name", defaultValue = "gdl") String name);
}
