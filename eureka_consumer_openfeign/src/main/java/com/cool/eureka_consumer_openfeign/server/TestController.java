package com.cool.eureka_consumer_openfeign.server;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {
    @Autowired
    TestServer testServer;

    @GetMapping("/sayHello/{name}")
    public String sayHello(@PathVariable("name") String name){
        return testServer.sayHello(name);
    }
}
