package com.cool.eureka_consumer_openfeign.Hystric;

import com.cool.eureka_consumer_openfeign.server.TestServer;
import org.springframework.stereotype.Component;

@Component
public class EurekaClientServiceHystric implements TestServer {
    @Override
    public String sayHello(String name) {
        return "调用eureka-client服务异常:调用"+name;
    }
}
